from re import X
from django.urls import path
from .models import Post
from . import views
from django.views.generic import ListView, DetailView

urlpatterns = [
    path('<int:pk>/delete', views.PostDeleteView.as_view(), name='post_delete'),
    path('<int:pk>/edit', views.UpdatePostView.as_view(), name='post_edit'),
    path('save/', views.save_post, name='save'),
    path('new/', views.add_post, name='new_post'),
    path('<int:pk>/', views.post, name='post'),
    path('', views.PostListView.as_view(), name='blog'),
]
