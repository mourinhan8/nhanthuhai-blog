from django.http.response import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.views.generic.edit import DeleteView, UpdateView
from .models import Post, Comment
from .forms import CommentForm, CreatePostForm
from django.http import HttpResponseRedirect
from django.views.generic import ListView, DetailView, CreateView
from django.urls import reverse_lazy, reverse
from django.shortcuts import redirect
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.
# hàm từ đặt
def list(request):
    Data = {'Posts': Post.objects.all().order_by('-date'), 'nav': 'blog'}
    return render(request, 'blog/blog.html', Data)

def post(request, pk):
    post = get_object_or_404(Post, pk=pk)
    # form = CommentForm()
    if request.method == 'POST':
        form = CommentForm(request.POST)
        # form = CommentForm(request.POST, author=request.user, post=post)
        if form.is_valid():
            # form.save()
            comment = form.save(commit=False)
            comment.post = post
            comment.author = request.user
            comment.save()
            return HttpResponseRedirect(request.path)
    else:
        form = CommentForm()
    return render(request, 'blog/post.html', {'post': post, 'form': form})

def add_post(request):
    form = CreatePostForm()
    return render(request, 'blog/create_post.html', {'form': form})

def save_post(request):
    if request.method == 'POST':
        form = CreatePostForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.save()
            return redirect(reverse('post', args=(str(post.id),)))
            # return HttpResponseRedirect(f'/blog/{post.id}')
        else:
            return HttpResponse('Khong duoc validate')
    else:
        return HttpResponse('Khong phai post request')

   
class UpdatePostView(LoginRequiredMixin, UpdateView):    
    model = Post
    template_name = 'blog/update_post.html'
    fields = ('title', 'body', 'image')

class PostDeleteView(LoginRequiredMixin ,DeleteView):
    model = Post
    template_name = 'blog/delete_post.html'
    success_url = reverse_lazy('blog')

# Sử dụng thư viện sẵn
# Thay hàm list
class PostListView(ListView):
    # queryset = Post.objects.all().order_by('-date')
    model = Post
    template_name = 'blog/blog.html'
    context_object_name = 'Posts'
    # bao nhiêu element được hiển thị tại đây
    paginate_by = 5

# Viết thêm đây để có kiến thức
# thay hàm post
class PostDetailView(DetailView):
    model = Post
    template_name = 'blog/post.html'

# Tạo post bằng class base view
class CreatePost(CreateView):
    model = Post
    template_name = 'blog/create_post.html'
    fields = '__all__'
