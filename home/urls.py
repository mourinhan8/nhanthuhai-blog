from django.urls import path
from . import views
from django.contrib.auth import logout, views as auth_views

urlpatterns = [
    path('', views.index, name='home'),
    path('contact/', views.contact, name='contact'),
]