from django.contrib.auth.models import AbstractUser
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db import models
from django.forms.fields import CharField
from django.utils.translation import gettext_lazy as _

# Create your models here.
class CustomUser(AbstractUser):
    username_validator = UnicodeUsernameValidator()

    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )
    first_name = None
    last_name = None
    fullname = models.CharField(null=True, blank=True, max_length=100)
    email = models.EmailField(
        _('email address'),
        unique=True,
        error_messages={
            'unique': _("A user with that email already exists."),
        }
    )
    address = models.CharField(_('address'), null=True,max_length=500, blank=True)
    year_birth = models.PositiveBigIntegerField(null=True, blank=True)
    biography = models.TextField(null=True, blank=True)
    avatar = models.URLField(null=True ,blank=True, max_length=1000)
    
    def __str__(self):
        return self.username
    
