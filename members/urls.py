from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    # path('profile/', views.ProfileView.as_view(), name='profile'),
    path('profile/', views.EditProfileView.as_view(), name='profile'),
    path('register/', views.SiteRegisterView.as_view(), name='register'),
    path('login/', views.SiteLoginView.as_view(), name='login'),
    # path('logout/', auth_views.LogoutView.as_view(next_page='/'), name='logout')
    path('logout/', views.SiteLogoutView.as_view(), name='logout')
]
