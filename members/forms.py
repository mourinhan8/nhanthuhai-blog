from django import forms
import re
from django.contrib.auth import get_user_model
# from .models import CustomUser as User
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.forms import UserChangeForm, UserCreationForm, UsernameField
from django.forms.fields import EmailField

User = get_user_model()

class RegistrationForm(forms.Form):
    instance = User
    username = forms.CharField(label='Tài khoản', max_length=30)
    email = forms.EmailField(label='Email')
    password1 = forms.CharField(label='Mật khẩu', widget=forms.PasswordInput())
    password2 = forms.CharField(label='Nhập lại mật khẩu', widget=forms.PasswordInput())
    # year_birth = forms.IntegerField(max_value=5000)

    def clean_password2(self):
        if 'password1' in self.cleaned_data:
            password1 = self.cleaned_data['password1']
            password2 = self.cleaned_data['password2']
            if password1 == password2 and password1:
                return password2
        raise forms.ValidationError("Mật khẩu không hợp lệ")
    
    def clean_username(self):
        username = self.cleaned_data['username']
        if not re.search(r'^\w+$', username):
            raise forms.ValidationError("Tên tài khoản có ký tự đặc biệt")
        try:
            User.objects.get(username=username)
        except ObjectDoesNotExist:
            return username
        raise forms.ValidationError("Tài khoản đã tồn tại")
    
    # def clean_year_birth(self):
    #     year_birth = self.cleaned_data['year_birth']
    #     if year_birth > 2021:
    #         raise forms.ValidationError("Năm nay mới 2021 thôi")
    #     return year_birth
    
    def clean_email(self):
        email = self.cleaned_data['email']
        try:
            User.objects.get(email=email)
        except ObjectDoesNotExist:
            return email
        raise forms.ValidationError("Email đã được sử dụng")

    def save(self):
        User.objects.create_user(
            username=self.cleaned_data['username'],
            email=self.cleaned_data['email'],
            password=self.cleaned_data['password1']
        )


class RegisterForm(UserCreationForm):
    email = EmailField(required=True)
    class Meta:
        model = User
        fields = ("username","email", "year_birth")
        # field_classes = {'username': UsernameField}
        # widgets = {
        #     # Tương tác với attribute của thẻ input html
        #     'email': forms.EmailInput(attrs={"required": True})
        # }

# Kế thừa UserChangeForm
class CustomUserChangeForm(UserChangeForm):

    class Meta:
        model = User
        fields = UserChangeForm.Meta.fields

# Kế thừa ModelForm
class UserEditForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('fullname', 'address', 'biography', 'year_birth')