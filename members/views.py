from django.contrib.auth import get_user_model
from django.contrib.auth.views import LoginView, LogoutView
from django.shortcuts import redirect, render, resolve_url
from django.urls.base import reverse_lazy
from django.views.generic.base import TemplateView
from django.views.generic import FormView
from django.views.generic.edit import UpdateView
from .forms import RegisterForm, RegistrationForm, UserEditForm
from django.http import HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.urls import reverse

User = get_user_model()

# Create your views here.
def register(request):
    form = RegistrationForm()
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse_lazy('login'))
    return render(request, 'users/register.html', {'form': form})

class SiteLoginView(LoginView):
    template_name = "users/login.html"

class SiteLogoutView(LogoutView):
    # next_page = '/'
    # template_name = "user/logout.html"
    pass

class ProfileView(LoginRequiredMixin, TemplateView):
    template_name = "users/profile.html"

# Cách khác edit profile
class EditProfileView1(LoginRequiredMixin, FormView):
    template_name = "users/profile.html"
    form_class = UserEditForm
    success_url = reverse_lazy("profile")

    def get_initial(self):
        user = self.request.user
        return {
            'fullname': user.fullname
        }
    
    def form_valid(self, form):
        from pprint import pprint; pprint(form.cleaned_data)
        form.save()
        return redirect(self.success_url)


class EditProfileView(LoginRequiredMixin, UpdateView):
    model = User
    template_name = "users/profile.html"
    fields = ('fullname', 'address', 'biography', 'year_birth')
    success_url = reverse_lazy("profile")

    def get_object(self, queryset=None):
        return self.request.user

class SiteRegisterView(FormView):
    template_name = "users/register.html"
    form_class = RegisterForm
    

    def form_valid(self, form):
        data = form.cleaned_data
        user = User.objects.create_user(
            username=data['username'], 
            password=data['password1'], 
            email=data['email']
            )
        from pprint import pprint; pprint(data)
        return redirect(reverse_lazy('login'))
        