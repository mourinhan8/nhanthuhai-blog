from django.contrib.auth import get_user_model
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .forms import RegisterForm, RegistrationForm, UserEditForm
from django.utils.translation import gettext, gettext_lazy as _

User = get_user_model()

# Register your models here.

# @admin.register(User)
class CustomUserAdmin(UserAdmin):
    list_display = ('username', 'email', 'fullname', 'year_birth', 'is_staff')
    add_form = RegisterForm
    form = UserEditForm
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {
            'fields': (
                'fullname',
                'email',
                'year_birth',
                'address',
                'biography',
            )
        }),
        (_('Permissions'), {
            'fields': (
                'is_active',
                'is_staff',
                'is_superuser',
                'groups',
                'user_permissions'
            ),
        }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'email', 'password1', 'password2'),
        }),
    )
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups')
    search_fields = ('username', 'fullname', 'email')
    ordering = ('username',)
    filter_horizontal = ('groups', 'user_permissions',)


admin.site.register(User, CustomUserAdmin)
